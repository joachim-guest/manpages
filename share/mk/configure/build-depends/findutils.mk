# Copyright 2022-2024, Alejandro Colomar <alx@kernel.org>
# SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception


ifndef MAKEFILE_CONFIGURE_BUILD_DEPENDS_FINDUTILS_INCLUDED
MAKEFILE_CONFIGURE_BUILD_DEPENDS_FINDUTILS_INCLUDED := 1


FIND    := find
XARGS   := xargs


endif  # include guard
